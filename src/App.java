import models.Circle;

public class App {
    public static void main(String[] args) throws Exception {
        Circle circle1 = new Circle();
        Circle circle2 = new Circle(3.0);

        System.out.println("Circle 1 có" );
        System.out.println("radius: " + circle1.getRadius());
        System.out.println("diện tích: " + circle1.getArea());
        System.out.println("chu vi: " + circle1.getCircumfernce());
        System.out.println(circle1.toString());


        System.out.println("Circle 2 có" );
        System.out.println("radius: " + circle2.getRadius());
        System.out.println("diện tích: " + circle2.getArea());
        System.out.println("chu vi: " + circle2.getCircumfernce());
        System.out.println(circle2.toString());
   
    }
}
